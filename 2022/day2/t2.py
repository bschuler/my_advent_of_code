#/usr/bin/python3

f = open("strategy_guide.txt", "r")

score = 0

new_outcome_points = {'X':0,'Y':3,'Z':6}
new_personal_points = {'A X':3,
                    'A Y':1,
                    'A Z':2,
                    'B X':1,
                    'B Y':2,
                    'B Z':3,
                    'C X':2,
                    'C Y':3,
                    'C Z':1}

lines = f.readlines()
for l in lines:
    line = l.rstrip()
    score += new_personal_points[line]
    (opponent_play, round_strategy) = line.split(" ")
    score += new_outcome_points[round_strategy]

f.close()
print(score)