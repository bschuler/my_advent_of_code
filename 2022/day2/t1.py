#/usr/bin/python3

f = open("strategy_guide.txt", "r")

score = 0

personal_points = {'X':1,'Y':2,'Z':3}
outcome_points = {'A X':3,
                  'A Y':6,
                  'A Z':0,
                  'B X':0,
                  'B Y':3,
                  'B Z':6,
                  'C X':6,
                  'C Y':0,
                  'C Z':3}

lines = f.readlines()
for l in lines:
    line = l.rstrip()
    score += outcome_points[line]
    (opponent_play, my_play) = line.split(" ")
    score += personal_points[my_play]

f.close()
print(score)