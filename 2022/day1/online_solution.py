def get_elf_list(item_list):
    elf_list = []
    elf = 0
    for item in item_list:
        if item:
            item = int(item)
            elf += item
        else:
            elf_list.append(elf)
            elf = 0
    elf_list.append(elf)
    return elf_list

def part_1_solution(day_1_input):
    elf_list = get_elf_list(day_1_input)
    max_elf = max(elf_list)
    max_elf_num = elf_list.index(max_elf)+1
    return max_elf_num, max_elf

def part_2_solution(day_1_input):
    elf_list = get_elf_list(day_1_input)
    elf_list.sort(reverse=True)
    print(elf_list[0])
    print(elf_list[1])
    print(elf_list[2])
    return elf_list[0] + elf_list[1] + elf_list[2]

# Getting input
f = open("calories_input.txt", "r")
lines = f.readlines()
day_1_input = [line.rstrip() for line in lines]
f.close()
   
# Finding Part 1 solution
print(part_1_solution(day_1_input))

# Finding Part 2 solution
print(part_2_solution(day_1_input))