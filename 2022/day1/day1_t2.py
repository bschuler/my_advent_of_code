#/usr/bin/python3

f = open("new_calories_input.txt", "r")

weights = 0
highest_weight = 0
line = 0
top3_weights = [0,0,0]
for x in range(1, 2250):
    line = f.readline()
    if line == '\n':
        if weights >= top3_weights[0]:
            top3_weights[2] = top3_weights[1]
            top3_weights[1] = top3_weights[0]
            top3_weights[0] = weights
        elif weights >= top3_weights[1]:
            top3_weights[2] = top3_weights[1]
            top3_weights[1] = weights
        else:
            if weights >= top3_weights[2]:
                top3_weights[2] = weights
        weights = 0
    else:
        weights += int(line)
    #print(f"x: {x}, value : {weights}")

f.close()
print(top3_weights[0])
print(top3_weights[1])
print(top3_weights[2])
highest_weight = top3_weights[0] + top3_weights[1] + top3_weights[2]
print(highest_weight)