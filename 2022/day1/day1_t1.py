#/usr/bin/python3

f = open("new_calories_input.txt", "r")

weights = 0
highest_weight = 0
line = 0
for x in range(1,2250):
    line = f.readline()
    if line == '\n':
        if weights >= highest_weight:
            highest_weight = weights  
        weights = 0
    else:
        weights += int(line)
    #print(f"x: {x}, value : {weights}")

f.close()
print(highest_weight)