#/usr/bin/python3

from math import floor

#f = open("test_rucksack.txt", "r")
f = open("rucksack_components.txt", "r")


score = 0

def val(char):
    if char.islower():
        return ord(char) - 96
    elif char.isupper():
        return ord(char) - 38

def remove(my_list, char):
    return my_list.replace(char,'')

lines = f.readlines()
for l in lines:
    line = l.rstrip()
    first_rucksack_limit = floor(len(line)/2)

    first_rucksack = line[0:first_rucksack_limit]
    second_rucksack = line[first_rucksack_limit:len(line)]

    for char in first_rucksack:
        if char in second_rucksack:
            score += val(char)
            second_rucksack = remove(second_rucksack, char)

f.close()
print(score)
