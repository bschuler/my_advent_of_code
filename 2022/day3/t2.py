#/usr/bin/python3

#f = open("test_rucksack.txt", "r")
f = open("rucksack_components.txt", "r")

score = 0
group = 0
groupline = ''

def val(char):
    if char.islower():
        return ord(char) - 96
    elif char.isupper():
        return ord(char) - 38

def remove(my_list, char):
    return my_list.replace(char,'')

lines = f.readlines()
for l in lines:
# for i in range(1,2):
#     l = lines[i]
    line = l.rstrip()
    group += 1
    if group == 1:
        groupline = line
    else:
        s1 = set(groupline)
        s2 = set(line)
        groupline = ''.join(list(s1&s2))
    if group >= 3:
        score+= val(groupline)
        group = 0
        groupline = ''

f.close()
print(score)
