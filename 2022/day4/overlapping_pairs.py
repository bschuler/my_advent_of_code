#/usr/bin/python3

f = open("assignments.txt", "r")
#f = open("test_assignments.txt", "r")

score = 0
group = 0
groupline = ''

def logic(ll1, ll2, lh1, lh2, score):
    if ll1 < ll2:
        if lh1 >= lh2:
            score+=1
    elif ll2 < ll1:
        if lh2 >= lh1:
            score+=1
    elif ll1 == ll2:
        score += 1
    elif lh1 == lh2:
        score += 1
    return score

lines = f.readlines()
for l in lines:
# for i in range(1,2):
#     l = lines[i]
    line = l.rstrip()
    (first_elf, second_elf) = l.split(',')
    (fe_low_limit, fe_high_limit) = first_elf.split('-')
    (se_low_limit, se_high_limit) = second_elf.split('-')
    # print(first_elf)
    # print(second_elf)

    score = logic(int(fe_low_limit), int(se_low_limit), int(fe_high_limit), int(se_high_limit), score)

f.close()
print(score)
